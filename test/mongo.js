const mongoose = require('mongoose');
const dotenv = require('dotenv');

dotenv.config();

const connectionUri = `mongodb://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/?authSource=${process.env.MONGO_AUTHENTICATION_DATABASE}`

mongoose.connect(connectionUri).then(() => {
    console.log('Connection successful! MongoDB connected.');;
    process.exit(0);
}).catch(e => {
    console.error(e);
    process.exit(1);
})