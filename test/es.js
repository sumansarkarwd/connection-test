const dotenv = require('dotenv');
const elasticsearch = require("elasticsearch");

dotenv.config();

const client = new elasticsearch.Client({
    hosts: [`${process.env.ES_HOST}:${process.env.ES_PORT}`],
    httpAuth: `${process.env.ES_USERNAME}:${process.env.ES_PASSWORD}`
});

client.cluster.health({}).then((health) => {
    console.log("Connection successful! Elasticsearch cluster health: " + health.status);
    process.exit(0);
}).catch(async (err) => {
    console.log(err);
    process.exit(1);
});