## Test connections

Clone the repo and copy .env.example as .env

1. `npm run test:es` - For elasticsearch connection test
2. `npm run test:mongo` - For MongoDB connection test